package dirdeb_charge

import "time"

type DirdebCharge struct {
	Host    string
	Path    string
	Timeout time.Duration
	Secret  string
	Debug   bool
	Prefix  string
}

type DirdebChargeCallback struct {
	Status           string      `json:"status"`
	PaymentID        string      `json:"payment_id"`
	Amount           string      `json:"amount" binding:"required"`
	Currency         string      `json:"currency"`
	Remarks          string      `json:"remarks" binding:"required"`
	DeviceID         string      `json:"device_id"`
	PaymentStatus    string      `json:"payment_status"`
	Reason           string      `json:"reason"`
	Location         Location    `json:"location"`
	Metadata         interface{} `json:"metadata"`
	LimitTransaction string      `json:"limit_transaction"`
}

type DirdebChargeResponse struct {
	StatusCode string
}

type Location struct {
	Lat string `sql:"not null" json:"lat"`
	Lon string `sql:"not null" json:"lon"`
}
