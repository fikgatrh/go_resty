package dirdeb_charge

import (
	"encoding/json"
	"fmt"
	"gopkg.in/resty.v1"
	"net/url"
	"time"
)

const defaultTimeout = 40 * time.Second

func NewClient(host string) *DirdebCharge {
	return &DirdebCharge{
		Host:    host,
		Timeout: defaultTimeout,
	}
}

func (dd *DirdebCharge) request() *resty.Request {
	return resty.SetDebug(true).
		SetRedirectPolicy(resty.FlexibleRedirectPolicy(15)).
		SetTimeout(dd.Timeout).
		R()
}

func (dd *DirdebCharge) SendNotificationCallback(dirdeb DirdebChargeCallback) (*DirdebChargeResponse, error) {
	u, _ := url.ParseRequestURI(dd.Host)
	////	u.Path = "/api/v1/directdebit/logs"
	urlStr := u.String()
	fmt.Println(urlStr,"ini url client")
	jsonString, err := json.Marshal(dirdeb)
	if err != nil {
		return nil, err
	}
	fmt.Println(string(jsonString), "ini isi body req")

	pr := &DirdebChargeResponse{}
	resp, err := dd.request().
		SetHeader("Content-Type", "application/json").
		SetBody(string(jsonString)).
		SetResult(pr).
		Post(urlStr)

	if err != nil {
		return nil, err
	}
	fmt.Println(resp)

	s := resp.String()
	if err := json.Unmarshal([]byte(s), pr); err != nil {
		return nil, err
	}

	return pr, nil
}
