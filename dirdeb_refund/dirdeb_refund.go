package dirdeb_refund

import (
	"encoding/json"
	"gopkg.in/resty.v1"
	"net/url"
	"time"
)

const defaultTimeout = 40 * time.Second

func NewClient(host string) *DirdebRefund {
	return &DirdebRefund{
		Host:    host,
		Timeout: defaultTimeout,
		//Debug:   os.Getenv("OTP_CLIENT_DEBUG") == "1",

	}
}

func (dd *DirdebRefund) request() *resty.Request {
	return resty.SetDebug(true).
		SetRedirectPolicy(resty.FlexibleRedirectPolicy(15)).
		SetTimeout(dd.Timeout).
		R()
}

func (dd *DirdebRefund) SendNotificationCallback(dirdeb DirdebRefundCallback) (*DirdebRefundResponse, error) {
	u, _ := url.ParseRequestURI(dd.Host)
	//	u.Path = "/api/v1/directdebit/logs"
	urlStr := u.String()
	jsonString, err := json.Marshal(dirdeb)
	if err != nil {
		return nil, err
	}

	pr := &DirdebRefundResponse{}
	resp, err := dd.request().
		SetHeader("Content-Type", "application/json").
		SetBody(string(jsonString)).
		SetResult(pr).
		Get(urlStr)

	if err != nil {
		return nil, err
	}

	s := resp.String()
	if err := json.Unmarshal([]byte(s), pr); err != nil {
		return nil, err
	}

	return pr, nil
}
