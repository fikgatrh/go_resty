package dirdeb_refund

import "time"

type DirdebRefund struct {
	Host    string
	Path    string
	Timeout time.Duration
	Secret  string
	Debug   bool
	Prefix  string
}

type DirdebRefundCallback struct {
	Status        string      `json:"status"`
	RefundID      string      `json:"refund_id"`
	PaymentID     string      `json:"payment_id"`
	Amount        string      `json:"amount" binding:"required"`
	Currency      string      `json:"currency"`
	Reason        string      `json:"reason"`
	RefundStatus  string      `json:"refund_status"`
	DeviceID      string      `json:"device_id"`
	Remarks       string      `json:"remarks" binding:"required"`
	PaymentStatus string      `json:"payment_status"`
	Location      Location    `json:"location"`
	Metadata      interface{} `json:"metadata"`
}

type DirdebRefundResponse struct {
	StatusCode string
}

type Location struct {
	Lat string `sql:"not null" json:"lat"`
	Lon string `sql:"not null" json:"lon"`
}
