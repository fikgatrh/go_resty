module go_resty

go 1.13

require (
	golang.org/x/net v0.0.0-20200707034311-ab3426394381 // indirect
	gopkg.in/resty.v1 v1.12.0
)
